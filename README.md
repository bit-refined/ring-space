Inspired by The Expanse, this mod adds a new sector which contains 1373 gates, of which a few are connected to existing
sectors in the galaxy.

# Existing Gates

Where possible, connections between existing gates have been cut to "chunk" the galaxy according to faction ownership
at game start. Some of these gates connections are routed to Ring Space instead.

# New Gates

The above chunking does not result in "fair" distribution of gates giving access to Ring Space, so to remedy
that some sectors have received additional gates that also connect to Ring Space.

# Ring Space

The sector currently has a diameter of 1853.317km. Each gate is at least 50km apart from any neighbouring gate.
There's no sunlight, no stars, planets or structures (would add a central station, but that's beyond my skills).
The sector is not excluded from faction logic and can be taken over by AI. Like in The Expanse, it is a sector of very
high economic value, where the value does not come from the contained resources (as there are none) but its central
"location" in relation to the rest of the galaxy. This will of course also attract the Xenon.

# Boron

A major part of the Kingdom End DLC is activating new gates. After extensive testing the plot never broke, however
Boron ships will be in the galaxy even before finishing the plot.

# Balance

Connecting usually far apart sectors by a now two-jump route is obviously throwing balance out the window. To make sure
it is not too overpowered, the sector is huge, giving ships immense travel times to other gates. In addition to that,
compared to Vanilla and all DLC, this is to my knowledge the only sector without any light, which makes self-sustaining
stations impossible as no energy cells can be produced.

# Future Work

This mod is currently a work in progress, but all the basics are there. Gate positions are not final and learning new
skills to add more features is ongoing.

# Thanks

ColdRogue26 helped play test most aspects to make sure basic functionality, integration and usage of the sector work as
expected. Thank you <3

# Contributions

If you'd like to help develop this mod further, e.g. by contributing models, new sectors, music or just ideas,
no matter how small they may seem, any help is appreciated. This is my first mod and I couldn't dive into every
aspect of development, so fleshing this out more with help from contributors and fans of The Expanse can only make this
mod better :)

---

Lastly as a tiny note, there's also something in this for fans of the X3 Hub, as that was also one of the inspirations.
