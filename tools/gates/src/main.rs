use std::collections::HashSet;
use std::path::Path;

use merkle_hash::{bytes_to_hex, Algorithm, MerkleTree};
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::SeedableRng;

use crate::ring_space::Gate;

use crate::gates::new::{
    BORON_GATES as NEW_BORON_GATES, PIRATE_GATES as NEW_PIRATE_GATES,
    SPLIT_GATES as NEW_SPLIT_GATES, TERRAN_GATES as NEW_TERRAN_GATES,
    VANILLA_GATES as NEW_VANILLA_GATES,
};
use crate::gates::removed::{
    BORON_CONNECTIONS, PIRATE_CONNECTIONS, SPLIT_CONNECTIONS, TERRAN_CONNECTIONS,
    VANILLA_CONNECTIONS,
};
use crate::gates::reused::{
    BORON_GATES as REUSED_BORON_GATES, PIRATE_GATES as REUSED_PIRATE_GATES,
    SPLIT_GATES as REUSED_SPLIT_GATES, TERRAN_GATES as REUSED_TERRAN_GATES,
    VANILLA_GATES as REUSED_VANILLA_GATES,
};

mod galaxy;
mod gates;
mod ring_space;
mod sector;
mod zones;

fn main() {
    let tree = MerkleTree::builder("./src/gates")
        .algorithm(Algorithm::Blake3)
        .hash_names(false)
        .build()
        .expect("indexing failed");
    let master_hash = tree.root.item.hash;
    let hash = bytes_to_hex(master_hash);

    let new_gates = [
        NEW_VANILLA_GATES,
        NEW_SPLIT_GATES,
        NEW_TERRAN_GATES,
        NEW_PIRATE_GATES,
        NEW_BORON_GATES,
    ]
    .concat();

    let cut_connections = [
        VANILLA_CONNECTIONS,
        SPLIT_CONNECTIONS,
        TERRAN_CONNECTIONS,
        PIRATE_CONNECTIONS,
        BORON_CONNECTIONS,
    ]
    .concat();

    let reused_gates = [
        REUSED_VANILLA_GATES,
        REUSED_SPLIT_GATES,
        REUSED_TERRAN_GATES,
        REUSED_PIRATE_GATES,
        REUSED_BORON_GATES,
    ]
    .concat();

    let (gates, _radius) = ring_space::generate_gates();

    println!(
        "Gates Center (this should be close to (0, 0, 0)): {:.5?}",
        ring_space::center_of_gates(&gates)
    );

    let best_gates = ring_space::best_gates_for_connections(&gates);
    println!(
        "Best {} gates to use so L and XL ships can use travel mode: {} to {}",
        best_gates.len(),
        best_gates.first().unwrap(),
        best_gates.last().unwrap()
    );

    println!("Gates with the best distribution around a 30° band:");

    let mut rng = SmallRng::seed_from_u64(42);

    let ring_placements = new_gates.len() + reused_gates.len();

    let mut distributed_gates: Vec<Gate> = Vec::with_capacity(ring_placements);

    let segment_size = 360. / ring_placements as f64;

    for segment in 0..ring_placements {
        let min_azimuth = -180. + (segment as f64 * segment_size);
        let max_azimuth = -180. + ((segment + 1) as f64 * segment_size);

        // We partition our "ball" in segments, one segment per gate.
        let valid_segment_gates = gates
            .iter()
            .filter(|gate| gate.yaw >= min_azimuth && gate.yaw < max_azimuth)
            .cloned()
            .collect::<Vec<_>>();

        // Then we make sure to only use gates with a valid pitch to make sure capital ships can
        // safely travel "up" and "down". We also declare that the first gate shall have a very
        // small pitch.
        let valid_pitch = if segment == 0 {
            -3.0..=3.0
        } else {
            -30.0..=30.
        };

        let valid_pitch_gates = valid_segment_gates
            .into_iter()
            .filter(|gate| valid_pitch.contains(&gate.pitch))
            .collect::<Vec<_>>();

        // Finally we need to also make sure that the newly selected gate is also at a good angle
        // to all other gates, so that travel between them can use the travel drive.
        let valid_travel_gates = valid_pitch_gates
            .into_iter()
            .filter(|gate| {
                distributed_gates.iter().all(|existing_gate| {
                    let elevation_angle = vertical_angle(
                        (existing_gate.x, existing_gate.z, existing_gate.y),
                        (gate.x, gate.z, gate.y),
                    );

                    (-45.0..45.0).contains(&elevation_angle)
                })
            })
            .collect::<Vec<_>>();

        let selected_gate = valid_travel_gates
            .choose(&mut rng)
            .expect("filtered out all possible gates :(");

        distributed_gates.push(selected_gate.clone());
    }

    distributed_gates.shuffle(&mut rng);

    let distributed_gate_ids = distributed_gates
        .into_iter()
        .map(|g| g.id)
        .collect::<Vec<_>>();

    assert_eq!(distributed_gate_ids.len(), distributed_gate_ids.iter().collect::<HashSet<_>>().len(), "distributed gate IDs need to be unique - this is just a check to make sure nothing breaks when changing the above code");

    println!("{:?}", distributed_gate_ids);

    println!("Generating Ring Space sector file...");
    ring_space::generate_sectors_xml(&gates);
    println!("Generating Ring Space zones file...");
    ring_space::generate_zones_xml(&gates, &hash);

    sector::generate(
        Path::new("../../maps/xu_ep2_universe/sectors.xml"),
        NEW_VANILLA_GATES,
        None,
    );
    zones::generate(
        Path::new("../../maps/xu_ep2_universe/zones.xml"),
        &hash,
        NEW_VANILLA_GATES,
    );

    sector::generate(
        Path::new("../../extensions/ego_dlc_split/maps/xu_ep2_universe/dlc4_sectors.xml"),
        NEW_SPLIT_GATES,
        Some(
            r#"    <!-- Note: We do it this way so the generated code below can add the required connection. -->
    <add sel="//macros">
        <macro name="Cluster_417_Sector8212_macro" class="sector">
            <component ref="standardsector"/>
            <connections>
            </connections>
        </macro>
    </add>"#,
        ),
    );
    zones::generate(
        Path::new("../../extensions/ego_dlc_split/maps/xu_ep2_universe/dlc4_zones.xml"),
        &hash,
        NEW_SPLIT_GATES,
    );

    sector::generate(
        Path::new("../../extensions/ego_dlc_terran/maps/xu_ep2_universe/dlc_terran_sectors.xml"),
        NEW_TERRAN_GATES,
        None,
    );
    zones::generate(
        Path::new("../../extensions/ego_dlc_terran/maps/xu_ep2_universe/dlc_terran_zones.xml"),
        &hash,
        NEW_TERRAN_GATES,
    );

    sector::generate(
        Path::new("../../extensions/ego_dlc_pirate/maps/xu_ep2_universe/dlc_pirate_sectors.xml"),
        NEW_PIRATE_GATES,
        None,
    );
    zones::generate(
        Path::new("../../extensions/ego_dlc_pirate/maps/xu_ep2_universe/dlc_pirate_zones.xml"),
        &hash,
        NEW_PIRATE_GATES,
    );

    sector::generate(
        Path::new("../../extensions/ego_dlc_boron/maps/xu_ep2_universe/dlc_boron_sectors.xml"),
        NEW_BORON_GATES,
        None,
    );
    zones::generate(
        Path::new("../../extensions/ego_dlc_boron/maps/xu_ep2_universe/dlc_boron_zones.xml"),
        &hash,
        NEW_BORON_GATES,
    );

    galaxy::generate(
        Path::new("../../maps/xu_ep2_universe/galaxy.xml"),
        &hash,
        &new_gates,
        &cut_connections,
        &reused_gates,
        &distributed_gate_ids,
    );
}

fn vertical_angle(p1: (f64, f64, f64), p2: (f64, f64, f64)) -> f64 {
    let dx = p2.0 - p1.0;
    let dy = p2.1 - p1.1;
    let dz = p2.2 - p1.2;
    let vertical_angle = dz.atan2((dx * dx + dy * dy).sqrt());
    vertical_angle.to_degrees()
}
