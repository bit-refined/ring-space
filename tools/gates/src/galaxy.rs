use std::path::Path;

use crate::gates::new::AdditionalGate;
use crate::gates::removed::Connection;
use crate::gates::reused::ReusedGate;

const BEGINNING: &str = r#"<?xml version="1.0" encoding="utf-8"?>
<!-- The following code is auto-generated. Do not touch. -->
<diff>"#;

const ENDING: &str = r#"    </add>
</diff>"#;

pub(crate) fn generate(
    file: &Path,
    hash: &str,
    new_gates: &[AdditionalGate],
    cut_connections: &[Connection],
    reused_gates: &[ReusedGate],
    gate_ids: &[usize],
) {
    println!("generating content for {}", file.display());

    assert_eq!(
        gate_ids.len(),
        new_gates.len() + reused_gates.len(),
        "exact amount of gate IDs is available for assignment"
    );

    let mut ids = gate_ids.iter();

    let mut content = format!("{BEGINNING}\n");

    for connection in cut_connections {
        let Connection { from, to, name } = connection;

        let cut = format!(
            r#"    <!-- Cut connection from {from} to {to} -->
    <remove sel="//macros/macro[@name='XU_EP2_universe_macro']/connections/connection[@name='{name}']"/>"#
        );

        content += &cut;
        content += "\n";
    }

    content += "\n";

    content += r#"    <add sel="/macros/macro[@name='XU_EP2_universe_macro']/connections">
        <!-- Add Ring Space Cluster to the standard universe -->
        <connection name="Ring_Space_Cluster_connection" ref="clusters">
            <offset>
                <position x="0" y="0" z="-173200000"/>
            </offset>
            <macro ref="Ring_Space_Cluster_macro" connection="galaxy"/>
        </connection>"#;

    content += "\n";

    for gate in new_gates {
        let AdditionalGate {
            name,
            cluster,
            sector,
            ..
        } = gate;

        let assigned_gate = ids
            .next()
            .expect("ran out of gate IDs - this should not happen");

        let addition = format!(
            r#"        <!-- Connect Ring Space to added gate in {name} -->
        <connection name="{name_escaped}_Ring_Space" ref="destination"
                    path="../Ring_Space_Cluster_connection/Ring_Space_Sector_connection/Ring_Space_Zone_{assigned_gate}_connection/Ring_Space_Gate_{assigned_gate}_connection_{hash}">
            <macro connection="destination"
                   path="../../../../../Cluster_{cluster:02}_connection/Cluster_{cluster:02}_Sector{sector:03}_connection/{name_escaped}_Ring_Space_Gate_Zone_connection/{name_escaped}_To_Ring_Space_connection_{hash}"/>
        </connection>"#,
            name_escaped = name.replace(' ', "_").replace("'s", "s")
        );
        content += &addition;
        content += "\n";
    }

    content += "\n";

    for gate in reused_gates {
        let ReusedGate {
            name,
            cluster,
            sector,
            zone,
            connection,
            ..
        } = gate;

        let assigned_gate = ids
            .next()
            .expect("ran out of gate IDs - this should not happen");

        let addition = format!(
            r#"        <!-- Connect Ring Space to existing gate in {name} -->
        <connection name="{name_escaped}_Ring_Space" ref="destination"
                    path="../Ring_Space_Cluster_connection/Ring_Space_Sector_connection/Ring_Space_Zone_{assigned_gate}_connection/Ring_Space_Gate_{assigned_gate}_connection_{hash}">
            <macro connection="destination"
                   path="../../../../../Cluster_{cluster:02}_connection/Cluster_{cluster:02}_Sector{sector:03}_connection/Zone{zone:03}_Cluster_{cluster:02}_Sector{sector:03}_connection/{connection}"/>
        </connection>"#,
            name_escaped = name.replace(' ', "_").replace("'s", "s")
        );
        content += &addition;
        content += "\n";
    }

    content += ENDING;

    assert!(
        ids.next().is_none(),
        "There should be no remaining unassigned gate IDs"
    );

    std::fs::write(file, content).expect("writing galaxy file failed")
}
