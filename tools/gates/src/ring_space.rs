use std::f64::consts::{FRAC_PI_2, PI};

// https://expanse.fandom.com/wiki/Slow_zone_(TV)
const GATES: usize = 1373;

// in meters
const MIN_DISTANCE: f64 = 50_000.;

// 1% within the above min distance is good enough.
const MIN_DISTANCE_MARGIN: f64 = 0.01;

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct Gate {
    pub(crate) id: usize,
    pub(crate) x: f64,
    pub(crate) y: f64,
    pub(crate) z: f64,
    pub(crate) yaw: f64,
    pub(crate) pitch: f64,
}

pub(crate) fn generate_gates() -> (Vec<Gate>, f64) {
    // Our initial guess for a good radius is to
    let mut r = MIN_DISTANCE;

    // The generated sphere might give the gates a bigger distance than needed,
    // so we adjust the radius until we're in a good enough margin.
    loop {
        let diameter = r * 2.;

        let gates = fibonacci_sphere(GATES, r)
            .into_iter()
            .enumerate()
            .map(|(i, (x, y, z))| {
                let yaw = (-z.atan2(x) - FRAC_PI_2).to_degrees();

                Gate {
                    id: i + 1,
                    x,
                    y,
                    z,
                    yaw: if yaw < -180. { yaw + 360. } else { yaw },
                    pitch: -(y / r).asin().to_degrees(),
                }
            })
            .collect::<Vec<_>>();

        let closest_distance = closest_gate_distance(&gates);

        if closest_distance > MIN_DISTANCE
            && closest_distance - MIN_DISTANCE <= MIN_DISTANCE * MIN_DISTANCE_MARGIN
        {
            assert_eq!(gates.len(), GATES);

            println!("Diameter of generated sphere: {:.3}km", diameter / 1000.);

            println!();

            println!(
                "Assuming maximum distance between two gates, these would be the travel times:"
            );
            println!("500m/s: {:.1}min", diameter / 500. / 60.);
            println!("1km/s: {:.1}min", diameter / 1000. / 60.);
            println!("2km/s: {:.1}min", diameter / 2000. / 60.);
            println!("5km/s: {:.1}min", diameter / 5000. / 60.);
            println!("7km/s: {:.1}min", diameter / 7000. / 60.);
            println!("10km/s: {:.1}min", diameter / 10000. / 60.);

            println!();

            println!("Closest gate distance: {:.5}", closest_distance);

            return (gates, r);
        }

        if closest_distance < MIN_DISTANCE {
            r *= 1.5
        } else {
            r /= 2.;
        }
    }
}

fn fibonacci_sphere(samples: usize, radius: f64) -> Vec<(f64, f64, f64)> {
    let mut points = Vec::with_capacity(samples);
    let phi = PI * (3. - 5_f64.sqrt()); // golden angle in radians

    for i in 0..samples {
        let y = radius * (1. - (i as f64 / (samples - 1) as f64) * 2.); // y goes from 1 to -1
        let r = (radius * radius - y * y).sqrt(); // radius at y

        let theta = phi * i as f64; // golden angle increment

        let x = theta.cos() * r;
        let z = theta.sin() * r;

        points.push((x, y, z));
    }

    points
}

pub(crate) fn generate_sectors_xml(gates: &[Gate]) {
    let beginning = r#"<?xml version="1.0" encoding="utf-8"?>
<!-- This file is generated. Do not edit. -->
<macros>
    <macro name="Ring_Space_Sector_macro" class="sector">
        <component ref="standardsector"/>
        <connections>"#;

    let mut connections = String::new();

    for gate in gates {
        let Gate { id, x, y, z, .. } = gate;

        let mut connection = format!(
            r#"            <connection name="Ring_Space_Zone_{id}_connection" ref="zones">
                <offset>
                    <position x="{x:.1}" y="{y:.1}" z="{z:.1}"/>
                </offset>
                <macro ref="Ring_Space_Zone_{id}_macro" connection="sector"/>
            </connection>"#
        );

        connection.push('\n');

        connections.push_str(&connection);
    }

    let ending = r#"        </connections>
    </macro>
</macros>"#;

    let file_contents = [beginning, &connections, ending].join("\n");

    std::fs::write(
        "../../maps/xu_ep2_universe/ring_space_sectors.xml",
        file_contents,
    )
    .expect("unable to write sectors file");
}

pub(crate) fn generate_zones_xml(gates: &[Gate], hash: &str) {
    let beginning = r#"<?xml version="1.0" encoding="utf-8"?>
<!-- This file is generated. Do not edit. -->
<macros>"#;

    let mut connections = String::new();

    for gate in gates {
        let Gate { id, yaw, pitch, .. } = gate;

        let mut connection = format!(
            r#"    <macro name="Ring_Space_Zone_{id}_macro" class="zone">
        <component ref="standardzone"/>
        <connections>
            <connection name="Ring_Space_Gate_{id}_connection_{hash}" ref="gates">
                <offset>
                    <position x="0" y="0" z="0"/>
                    <rotation yaw="{yaw:.3}" pitch="{pitch:.3}" roll="0"/>
                </offset>
                <macro ref="props_gates_anc_gate_macro" connection="space"/>
            </connection>
        </connections>
    </macro>"#
        );

        connection.push('\n');

        connections.push_str(&connection);
    }

    let ending = r#"</macros>"#;

    let file_contents = [beginning, &connections, ending].join("\n");

    std::fs::write(
        "../../maps/xu_ep2_universe/ring_space_zones.xml",
        file_contents,
    )
    .expect("unable to write zones file");
}

pub(crate) fn center_of_gates(points: &[Gate]) -> (f64, f64, f64) {
    let n = points.len() as f64;
    let mut x_sum = 0.0;
    let mut y_sum = 0.0;
    let mut z_sum = 0.0;

    for gate in points {
        x_sum += gate.x;
        y_sum += gate.y;
        z_sum += gate.z;
    }

    (x_sum / n, y_sum / n, z_sum / n)
}

fn closest_gate_distance(gates: &[Gate]) -> f64 {
    let mut closest = f64::MAX;

    for gate_a in gates {
        for gate_b in gates {
            if gate_a == gate_b {
                continue;
            }
            let distance = distance_between_gates(gate_a, gate_b);

            if distance < closest {
                closest = distance;
            }
        }
    }

    closest
}

fn distance_between_gates(gate_a: &Gate, gate_b: &Gate) -> f64 {
    distance_between_points(
        (gate_a.x, gate_a.y, gate_a.z),
        (gate_b.x, gate_b.y, gate_b.z),
    )
}

pub(crate) fn distance_between_points(point_a: (f64, f64, f64), point_b: (f64, f64, f64)) -> f64 {
    let x_diff = point_b.0 - point_a.0;
    let y_diff = point_b.1 - point_a.1;
    let z_diff = point_b.2 - point_a.2;

    (x_diff.powi(2) + y_diff.powi(2) + z_diff.powi(2)).sqrt()
}

pub(crate) fn best_gates_for_connections(gates: &[Gate]) -> Vec<usize> {
    gates
        .iter()
        .filter(|gate| gate.pitch >= -30. && gate.pitch <= 30.)
        .map(|gate| gate.id)
        .collect()
}
