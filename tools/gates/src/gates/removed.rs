#[derive(Clone)]
pub(crate) struct Connection {
    pub(crate) from: &'static str,
    pub(crate) to: &'static str,
    pub(crate) name: &'static str,
}

pub(crate) const VANILLA_CONNECTIONS: &[Connection] = &[
    Connection {
        from: "Trinity Sanctum III",
        to: "Hewa's Twin I",
        name: "ClusterGate018To019",
    },
    Connection {
        from: "Second Contact II Flashpoint",
        to: "Argon Prime",
        name: "ClusterGate013To014",
    },
    Connection {
        from: "Morning Star III",
        to: "Heretics End",
        name: "ClusterGate030To031",
    },
    Connection {
        from: "Bright Promise",
        to: "Trinity Sanctum VII",
        name: "ClusterGate009To018",
    },
    Connection {
        from: "Grand Exchange III",
        to: "Black Hole Sun V",
        name: "ClusterGate001To006",
    },
    Connection {
        from: "Grand Exchange I",
        to: "Nopileos Fortune VI",
        name: "ClusterGate001To004",
    },
    Connection {
        from: "Silent Witness I",
        to: "Profit Center Alpha",
        name: "ClusterGate008To034",
    },
    Connection {
        from: "Eighten Billion",
        to: "Silent Witness I",
        name: "ClusterGate002To008",
    },
    Connection {
        from: "Hatikvahs Choice I",
        to: "Tharkas Cascade XV",
        name: "ClusterGate029To032",
    },
];
pub(crate) const SPLIT_CONNECTIONS: &[Connection] = &[
    Connection {
        from: "Guiding Star V",
        to: "Two Grand",
        name: "ClusterGate416To420",
    },
    Connection {
        from: "Two Grand",
        to: "Fires of Defeat",
        name: "ClusterGate420To421",
    },
    Connection {
        from: "Zyarths Dominion",
        to: "Open Market",
        name: "ClusterGate405To419",
    },
    Connection {
        from: "Family Nhuut",
        to: "Open Market",
        name: "ClusterGate418To419",
    },
    Connection {
        from: "Wretched Skies V Family Phi",
        to: "Heretics End",
        name: "ClusterGate403To031",
    },
    Connection {
        from: "Thuruks Demise",
        to: "Ianamus Zura",
        name: "ClusterGate408To015",
    },
    Connection {
        from: "Family Zhin",
        to: "Tharkas Cascade XV",
        name: "ClusterGate401To032",
    },
];
pub(crate) const TERRAN_CONNECTIONS: &[Connection] = &[
    Connection {
        from: "Neptune",
        to: "Brennan's Triumph",
        name: "ClusterGate110To115",
    },
    Connection {
        from: "Segaris",
        to: "Antigone Memorial",
        name: "ClusterGate113To028",
    },
];
pub(crate) const PIRATE_CONNECTIONS: &[Connection] = &[
    Connection {
        from: "Windfall IV Auroras Dream",
        to: "Black Hole Sun IV",
        name: "ClusterGate503To006",
    },
    Connection {
        from: "Windfall I Union Summit",
        to: "Silent Witness I",
        name: "ClusterGate501To008",
    },
    Connection {
        from: "Windfall I Union Summit",
        to: "Eighteen Billion",
        name: "ClusterGate501To002",
    },
];
pub(crate) const BORON_CONNECTIONS: &[Connection] = &[];
