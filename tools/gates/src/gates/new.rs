#[derive(Clone)]
pub(crate) struct AdditionalGate {
    pub(crate) name: &'static str,
    pub(crate) cluster: usize,
    pub(crate) sector: usize,
    pub(crate) x: f64,
    pub(crate) y: f64,
    pub(crate) z: f64,
    pub(crate) yaw: f64,
}

pub(crate) const VANILLA_GATES: &[AdditionalGate] = &[
    AdditionalGate {
        name: "Second Contact VII",
        cluster: 40,
        sector: 1,
        x: -250_000.0,
        y: 0.0,
        z: -100_000.0,
        yaw: 90.0,
    },
    AdditionalGate {
        name: "Lasting Vengeance",
        cluster: 35,
        sector: 1,
        x: 300_000.0,
        y: 0.0,
        z: -250_000.0,
        yaw: -90.0,
    },
    AdditionalGate {
        name: "Pious Mists XI",
        cluster: 38,
        sector: 1,
        x: -50000.0,
        y: 0.0,
        z: -300000.0,
        yaw: 0.0,
    },
    AdditionalGate {
        name: "Memory of Profit X",
        cluster: 39,
        sector: 1,
        x: 0.0,
        y: 0.0,
        z: -600000.,
        yaw: 0.0,
    },
    AdditionalGate {
        name: "Hewa's Twin V",
        cluster: 43,
        sector: 1,
        x: 0.0,
        y: 0.0,
        z: 300_000.0,
        yaw: 180.0,
    },
    AdditionalGate {
        name: "Heretics End",
        cluster: 31,
        sector: 1,
        x: -200_000.,
        y: 0.0,
        z: -200_000.,
        yaw: 35.0,
    },
];

pub(crate) const SPLIT_GATES: &[AdditionalGate] = &[
    // Placed inside the Terraformer HUB
    AdditionalGate {
        name: "Eleventh Hour",
        cluster: 417,
        sector: 8212,
        x: 0.,
        y: 0.,
        z: 0.,
        yaw: 180.0,
    },
    AdditionalGate {
        name: "Tharka's Ravine XXIV",
        cluster: 409,
        sector: 1,
        x: 200_000.,
        y: 0.0,
        z: -300_000.0,
        yaw: -30.,
    },
    AdditionalGate {
        name: "Family Nhuut",
        cluster: 418,
        sector: 1,
        x: -50_000.0,
        y: 0.0,
        z: -200_000.0,
        yaw: 30.0,
    },
];

pub(crate) const TERRAN_GATES: &[AdditionalGate] = &[
    AdditionalGate {
        name: "Uranus",
        cluster: 109,
        sector: 1,
        x: -700_000.,
        y: 0.0,
        z: -100_000.,
        yaw: 90.,
    },
    AdditionalGate {
        name: "Gaian Prophecy",
        cluster: 114,
        sector: 1,
        x: 0.0,
        y: 0.0,
        z: -400_000.,
        yaw: 0.,
    },
];

pub(crate) const PIRATE_GATES: &[AdditionalGate] = &[
    AdditionalGate {
        name: "Windfall III The Hoard",
        cluster: 502,
        sector: 1,
        x: -300_000.0,
        y: 0.0,
        z: 0.0,
        yaw: 90.0,
    },
    AdditionalGate {
        name: "Unknown System",
        cluster: 504,
        sector: 1,
        x: 0.0,
        y: 0.0,
        z: -300_000.0,
        yaw: 0.0,
    },
];

pub(crate) const BORON_GATES: &[AdditionalGate] = &[
    AdditionalGate {
        name: "Ocean of Fantasy",
        cluster: 604,
        sector: 1,
        x: 0.0,
        y: 0.0,
        z: -300_000.0,
        yaw: 0.0,
    },
    AdditionalGate {
        name: "Rolk's Demise",
        cluster: 607,
        sector: 1,
        x: -200_000.0,
        y: 0.0,
        z: 30_000.0,
        yaw: 135.0,
    },
];
