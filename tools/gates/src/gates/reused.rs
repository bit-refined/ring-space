#[derive(Clone)]
pub(crate) struct ReusedGate {
    pub(crate) name: &'static str,
    pub(crate) cluster: usize,
    pub(crate) sector: usize,
    pub(crate) zone: usize,
    pub(crate) connection: &'static str,
}

pub(crate) const VANILLA_GATES: &[ReusedGate] = &[
    ReusedGate {
        name: "Argon Prime",
        cluster: 14,
        sector: 1,
        zone: 1,
        connection: "connection_ClusterGate014To013",
    },
    ReusedGate {
        name: "Black Hole Sun V",
        cluster: 6,
        sector: 2,
        zone: 4,
        connection: "connection_ClusterGate006To001",
    },
    ReusedGate {
        name: "Tharkas Cascade XV",
        cluster: 32,
        sector: 1,
        zone: 4,
        connection: "connection_ClusterGate032To029",
    },
];
pub(crate) const SPLIT_GATES: &[ReusedGate] = &[ReusedGate {
    name: "Guiding Star V",
    cluster: 416,
    sector: 1,
    zone: 3,
    connection: "connection_ClusterGate416To420",
}];
pub(crate) const TERRAN_GATES: &[ReusedGate] = &[];
pub(crate) const PIRATE_GATES: &[ReusedGate] = &[];
pub(crate) const BORON_GATES: &[ReusedGate] = &[];
