use std::path::Path;

use crate::gates::new::AdditionalGate;

const BEGINNING: &str = r#"<?xml version="1.0" encoding="utf-8"?>
<!-- The following code is auto-generated. Do not touch. -->
<diff>"#;

const ENDING: &str = r#"</diff>"#;

pub(crate) fn generate(file: &Path, gates: &[AdditionalGate], additional_xml: Option<&str>) {
    println!("generating content for {}", file.display());

    let mut content = format!("{BEGINNING}\n");

    if let Some(xml) = additional_xml {
        content += xml;
        content += "\n";
    }

    for gate in gates {
        let AdditionalGate {
            name,
            cluster,
            sector,
            x,
            y,
            z,
            ..
        } = gate;

        let addition = format!(
            r#"    <!-- Add an extra zone to {name}, so we can place a new gate far away. -->
    <add sel="//macros/macro[@name='Cluster_{cluster:02}_Sector{sector:03}_macro']/connections">
        <connection name="{name_escaped}_Ring_Space_Gate_Zone_connection" ref="zones">
            <offset>
                <position x="{x}" y="{y}" z="{z}"/>
            </offset>
            <macro ref="{name_escaped}_Ring_Space_Gate_Zone_macro" connection="sector"/>
        </connection>
    </add>"#,
            name_escaped = name.replace(' ', "_").replace("'s", "s")
        );
        content += &addition;
        content += "\n";
    }

    content += ENDING;

    std::fs::write(file, content).expect("writing sectors file failed")
}
