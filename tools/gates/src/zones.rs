use std::path::Path;

use crate::gates::new::AdditionalGate;

const BEGINNING: &str = r#"<?xml version="1.0" encoding="utf-8"?>
<!-- The following code is auto-generated. Do not touch. -->
<diff>"#;

const ENDING: &str = r#"</diff>"#;

pub(crate) fn generate(file: &Path, hash: &str, gates: &[AdditionalGate]) {
    println!("generating content for {}", file.display());

    let mut content = format!("{BEGINNING}\n");

    for gate in gates {
        let AdditionalGate { name, yaw, .. } = gate;

        let addition = format!(
            r#"    <!-- {name} to Ring Space Jump Gate -->
    <add sel="//macros">
        <macro name="{name_escaped}_Ring_Space_Gate_Zone_macro" class="zone">
            <component ref="standardzone"/>
            <connections>
                <connection name="{name_escaped}_To_Ring_Space_connection_{hash}" ref="gates">
                    <offset>
                        <position x="0" y="0" z="0"/>
                        <rotation yaw="{yaw}" pitch="0" roll="0"/>
                    </offset>
                    <macro ref="props_gates_anc_gate_macro" connection="space"/>
                </connection>
            </connections>
        </macro>
    </add>"#,
            name_escaped = name.replace(' ', "_").replace("'s", "s")
        );
        content += &addition;
        content += "\n";
    }

    content += ENDING;

    std::fs::write(file, content).expect("writing zones file failed")
}
